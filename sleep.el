;;; sleep.el  --- Sleep and locking controls -*- lexical-binding: t -*-

;;; Commentary:
;;; Functions for controlling sleep and screen locking on multiple operating systems.

;;; Code:

(defcustom lock-command
  (cond
   ((executable-find "xsecurelock") "xsecurelock")
   ((executable-find "xlock") "xlock")
   (t nil))
  "Command to execute when running screen locking functions."
  :group 'sleep
  :type 'string)

(defun os-type ()
  (cond
   ((string-search "linux" system-configuration) 'linux)
   ((string-search "freebsd" system-configuration) 'freebsd)
   ((string-search "openbsd" system-configuration) 'openbsd)))

(defun sleep--do-sleep (sleep-type)
  "Perform SLEEP-TYPE (one of \='light \='deep or \='hibernation)."
  (pcase (os-type)
    ('linux (let ((statefile "/sys/power/state"))
              (when (file-writable-p statefile)
                (with-temp-file statefile
                  (insert (pcase sleep-type
                            ('light "freeze")
                            ('deep "mem")
                            ('hibernation "disk")))))))
    ('openbsd (shell-command
               (format "apm %s" (pcase sleep-type
                                  ('light "-S")
                                  ('deep "-z")
                                  ('hibernation "-Z")))))))

(defun sleep--select-sleep-type ()
  "Interactively select possible sleep types."
  (let ((data '(("disk (hibernation)" . hibernation)
                ("suspend (light sleep)" . light)
                ("memory (deep sleep)" . deep))))
    (cdr (assoc (completing-read "Sleep Type: " data #'identity t) data))))

;;;###autoload
(defun sleep (&optional sleep-type)
  "Call do-sleep with SLEEP-TYPE if it's non-nil.
Otherwise, interactively select a sleep-type."
  (interactive)
  (sleep--do-sleep (or sleep-type (sleep--select-sleep-type))))


;;;###autoload
(defun lock ()
  "Lock screen by executing LOCK-COMMAND."
  (interactive)
  (shell-command lock-command))

;;;###autoload
(defun lock-and-sleep ()
  "Lock command and subsequently enter sleep."
  (interactive)
  (let ((sleep-type (sleep--select-sleep-type)))
    (async-shell-command lock-command)
    (sleep-for 1)
    (sleep--do-sleep sleep-type)))

(provide 'sleep)
;;; sleep.el ends here
